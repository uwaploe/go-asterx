package asterx

import (
	"bytes"
	"testing"
)

func TestScanner(t *testing.T) {
	table := []struct {
		input, output string
	}{
		{
			input:  "$R: geth\r\n  EthernetMode, off\r\nUSB1>",
			output: "$R: geth\r\n  EthernetMode, off\r\n",
		},
		{input: "USB1>", output: ""},
	}

	var rbuf bytes.Buffer
	s := newScanner(&rbuf, "USB1>")
	for _, e := range table {
		rbuf.Write([]byte(e.input))
		resp, _ := s.next()
		if resp != e.output {
			t.Errorf("Mismatch; expected %q, got %q", e.output, resp)
		}
	}

}

func TestFindError(t *testing.T) {
	resp := "$R? foo: Invalid command!\r\n>"
	err := findError(resp)
	val, ok := err.(*AsterxError)
	if !ok {
		t.Fatal("Error not detected")
	}
	if val.text != "foo: Invalid command!" {
		t.Errorf("Wrong error text: %q", val.text)
	}
}
