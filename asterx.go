// Package asterx provides an interface to Septentrio AsterX GNSS receivers
package asterx

import (
	"bufio"
	"bytes"
	"io"
	"strings"

	"github.com/pkg/errors"
)

const (
	// Command terminator
	EOT = "\r"
	// Response line terminator
	EOL = "\r\n"
)

// respScanner parses the command responses from the AsterX receiver
type respScanner struct {
	rdr     io.Reader
	respBuf bytes.Buffer
	prompt  []byte
	tail    int
}

func newScanner(rdr io.Reader, prompt string) *respScanner {
	return &respScanner{
		rdr:    rdr,
		prompt: []byte(prompt),
	}
}

func (s *respScanner) next() (string, error) {
	s.respBuf.Reset()
	s.tail = -len(s.prompt)
	b := make([]byte, 1)
	for {
		_, err := s.rdr.Read(b)
		if err != nil {
			return s.respBuf.String(), err
		}

		s.respBuf.Write(b)
		s.tail++
		if s.tail < 0 {
			continue
		}

		if bytes.Equal(s.respBuf.Bytes()[s.tail:], s.prompt) {
			return strings.TrimSuffix(s.respBuf.String(), string(s.prompt)), nil
		}

	}

	return "", nil
}

// AsterxError represents an error message returned by the device.
type AsterxError struct {
	text string
}

func (e *AsterxError) Error() string {
	return e.text
}

// Device provides a command-control/data interface to a Septentrio GNSS
// receiver.
type Device struct {
	port    io.ReadWriter
	scanner *respScanner
}

// New returns a new *Device instance attached to a ReadWriter. Prompt is
// the command prompt issued by the device.
func New(rw io.ReadWriter, prompt string) *Device {
	return &Device{
		port:    rw,
		scanner: newScanner(rw, prompt),
	}
}

func findError(resp string) error {
	if idx := strings.Index(resp, "$R? "); idx >= 0 {
		if end := strings.Index(resp[idx:], EOL); end >= 0 {
			return &AsterxError{
				text: strings.Trim(resp[idx+4:idx+end], " "),
			}
		}
	}
	return nil
}

// GetPrompt is used to test the command interface
func (d *Device) GetPrompt() error {
	d.port.Write([]byte("#x" + EOT))
	_, err := d.scanner.next()
	return err
}

// Command sends a command to the device and returns the response along with
// any error that occurred.
func (d *Device) Command(req string) (string, error) {
	_, err := d.port.Write([]byte(req + EOT))
	if err != nil {
		return "", err
	}

	resp, err := d.scanner.next()
	if err == nil {
		err = findError(resp)
	}

	if err == nil {
		// Skip over the first line containing the echoed command.
		if idx := strings.Index(resp, EOL); idx >= 0 {
			return resp[idx+len(EOL):], nil
		}
	}

	return resp, err
}

// Upload sends the contents of a command file to the device.
func (d *Device) Upload(cmdfile io.Reader) error {
	scanner := bufio.NewScanner(cmdfile)
	line := int(0)
	for scanner.Scan() {
		line++
		if scanner.Bytes()[0] == '#' {
			// Skip comment lines
			continue
		}
		cmd := strings.TrimRight(scanner.Text(), " \t\r\n")
		_, err := d.Command(cmd)
		if err != nil {
			return errors.Wrapf(err, "line %d", line)
		}
	}

	return scanner.Err()
}
